import React from "react";
import Modal from "react-modal";
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Redirect,
} from "react-router-dom";

import * as ROUTES from "./constants/router";
import { NavbarContainer } from "./container";
import { Layanan, Pengumuman, HKO, KBMN, PEP, UH } from "./pages";

Modal.setAppElement("#root");

export default function App() {
	return (
		<Router>
			<NavbarContainer />
			<Redirect from={ROUTES.HOME} to={ROUTES.LAYANAN} />
			<Switch>
				<Route exact path={ROUTES.LAYANAN}>
					<Layanan />
				</Route>
				<Route exact path={ROUTES.PENGUMUMAN}>
					<Pengumuman />
				</Route>
				<Route exact path={ROUTES.HUKUM_KEPEGAWAIAN_DAN_ORGANISASI}>
					<HKO />
				</Route>
				<Route exact path={ROUTES.KEUANGAN_DAN_BARANG_MILIK_NEGARA}>
					<KBMN />
				</Route>
				<Route exact path={ROUTES.PERENCANAAN_EVALUASI_DAN_PELAPORAN}>
					<PEP />
				</Route>
				<Route exact path={ROUTES.UMUM_DAN_HUMAS}>
					<UH />
				</Route>
			</Switch>
		</Router>
	);
}
