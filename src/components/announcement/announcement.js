import styled from 'styled-components/macro';

export const Container = styled.div`  
  display: flex;
  flex-direction: column;
  width: 650px;
  padding: 24px 32px;
  margin-top: 1rem;
  margin-bottom: 1rem;
  margin-right: 3rem;
  background: #ffffff;
  border: 1px solid #dedede;
  border-radius: 6px;
  text-decoration: none;
  cursor: pointer;

  &:focus, &:visited, &:link, &:active {
    text-decoration: none;
  }

  &:hover {
    box-shadow:
    0 2.8px 2.2px rgba(0, 0, 0, 0.01),
    0 6.7px 5.3px rgba(0, 0, 0, 0.02),
    0 12.5px 10px rgba(0, 0, 0, 0.03);
  }
`;

export const Wrapper = styled.div``;

export const Title = styled.h1`
  font-size: 20px;
  font-weight: 600;
  margin: 0;
  padding: 0;
`;

export const Tag = styled.button`
  background: #ECFDF5;
  color: #10B981;
  font-size: 16px;
  margin-top: 12px;
  padding: 8px 16px;
  border: none;
  outline: none;
`;

export const Text = styled.p`
  margin-top: 32px;
  margin-bottom: 0px;
  padding: 0;
  font-size: 16px;
  color: #4d4d4d;
`;