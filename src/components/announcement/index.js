import React from 'react';
import {
  Container, Wrapper, Title, Tag, Text
} from './announcement';

export default function Announcement({ children, ...restProps }) {
  return <Container {...restProps}> {children} </Container>;
};

Announcement.Wrapper = function AnnouncementWrapper({ children, ...restProps }) {
  return <Wrapper {...restProps}> { children } </Wrapper>;
};

Announcement.Title = function AnnouncementTitle({ children, ...restProps }) {
  return <Title {...restProps}> {children} </Title>;
};

Announcement.Tag = function AnnouncementTag({ children, ...restProps }) {
  return <Tag {...restProps}> {children} </Tag>;
};

Announcement.Text = function AnnouncementText({ children, ...restProps }) {
  return <Text {...restProps}> {children} </Text>;
};
