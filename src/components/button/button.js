import styled from "styled-components/macro";
import { Link } from "react-router-dom";
import { IoIosArrowBack } from "react-icons/io";

export const Container = styled.div`
	display: flex;
	flex-direction: row-reverse;
`;

export const Back = styled(Link)`
	display: flex;
	justify-content: center;
	align-items: center;
	width: 100px;
	background: #6ee7b7;
	padding: 12px 16px;
	margin-top: 32px;
	border: none;
	border-radius: 2px;
	font-size: 18px;
	color: #333333;
	opacity: 1;

	&:hover {
		opacity: 0.9;
	}

	&:link {
		text-decoration: none;
	}
`;

export const Icon = styled(IoIosArrowBack)`
	font-size: 18px;
	margin-right: 4px;
	opacity: 1;

	&:hover {
		opacity: 0.9;
	}
`;
