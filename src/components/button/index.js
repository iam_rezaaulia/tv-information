import React from "react";
import { Container, Back, Icon } from "./button";

export default function Button({ children, ...rest }) {
	return <Container {...rest}> {children} </Container>;
}

Button.ButtonBack = function ButtonBack({ children, ...rest }) {
	return <Back {...rest}> {children} </Back>;
};

Button.IconBack = function ButtonIconBack() {
	return <Icon />;
};
