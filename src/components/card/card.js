import styled from "styled-components/macro";
import { Link } from "react-router-dom";

export const Wrapper = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
	align-items: center;
`;

export const Text = styled.p`
	font-size: 18px;
	font-weight: 500;
	line-height: 1.4rem;
	margin-top: 38px;
	margin-bottom: 0;
	text-align: center;
	word-wrap: break-word;
	text-decoration: none;
	color: #333333;
`;

export const TextSmall = styled.p`
	font-size: 15px;
	font-weight: 400;
	margin-top: 12px;
	margin-bottom: 0;
	padding: 0;
	text-align: center;
	word-wrap: break-word;
	color: #aaaaaa;
`;

export const Container = styled(Link)`
	display: flex;
	flex-direction: column;
	align-items: center;
	margin: 48px 48px 0;
	padding: 42px 0;
	background: #ffffff;
	border: 1px solid #dedede;
	border-radius: 6px;
	width: 300px;
	height: 200px;
	text-decoration: none;
	cursor: pointer;
	transition: all 300ms ease-in-out;

	&:hover {
		box-shadow: 0 2.8px 2.2px rgba(0, 0, 0, 0.01),
			0 6.7px 5.3px rgba(0, 0, 0, 0.02), 0 12.5px 10px rgba(0, 0, 0, 0.03);
	}
`;

export const Image = styled.img`
	background-color: #f5f6f8;
	border-radius: 12px;
	padding: 10px;
	width: 86px;
	height: auto;
`;
