import React from 'react';
import { Wrapper, Container, Image, Text, TextSmall } from './card';

export default function Card({ children, ...restProps }) {
  return <Wrapper {...restProps}> {children} </Wrapper>;
};

Card.Container = function CardContainer({ children, ...restProps }) {
  return <Container {...restProps}> {children} </Container>;
};

Card.Image = function CardImage({ children, ...restProps }) {
  return <Image {...restProps} />;
};

Card.Text = function CardText({ children, ...restProps }) {
  return <Text {...restProps}> {children} </Text>;
};

Card.TextSmall = function CardTextSmall({ children, ...restProps }) {
  return <TextSmall {...restProps}> {children} </TextSmall>;
};
