import styled from "styled-components/macro";

export const Container = styled.div`
	margin: 0 auto;
	padding: 0;
	transition: all 300ms ease-in;
`;

export const Title = styled.h1`
	text-align: center;
	font-size: 36px;
	font-weight: 400;
	color: #333333;
	margin-top: 0;
	margin-bottom: 8px;
`;

export const Span = styled.span`
	background: #6ee7b7;
	font-size: 36px;
	font-weight: 300;
	padding: 8px 32px;
	border-radius: 2px;
	color: #333333;
`;
