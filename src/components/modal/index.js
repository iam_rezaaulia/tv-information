import React from "react";
import { ModalProvider } from "styled-react-modal";
import {
	Container,
	Background,
	Title,
	Subtitle,
	Text,
	Ol,
	Li,
	Image,
	Frame,
	Wrapper,
	Table,
	TableWrapper,
	TableHeader,
	TableContent,
	Break,
} from "./modal";

export default function Modal({ children, ...restProps }) {
	return (
		<ModalProvider backgroundComponent={Background} {...restProps}>
			{children}
		</ModalProvider>
	);
}

Modal.Container = function ModalContainer({
	height = "auto",
	overflow = "none",
	children,
	...restProps
}) {
	return (
		<Container height={height} overflow={overflow} {...restProps}>
			{children}
		</Container>
	);
};

Modal.Title = function ModalTitle({ children, ...restProps }) {
	return <Title {...restProps}> {children} </Title>;
};

Modal.Subtitle = function ModalSubtitle({ children, ...restProps }) {
	return <Subtitle {...restProps}> {children} </Subtitle>;
};

Modal.Text = function ModalText({ children, ...restProps }) {
	return <Text {...restProps}> {children} </Text>;
};

Modal.Ol = function ModalOl({ children, ...restProps }) {
	return <Ol {...restProps}> {children} </Ol>;
};

Modal.Li = function ModalLi({ children, ...restProps }) {
	return <Li {...restProps}> {children} </Li>;
};

Modal.Image = function ModalImage({ ...restProps }) {
	return <Image {...restProps} />;
};

Modal.Frame = function ModalFrame({ children, ...restProps }) {
	return <Frame {...restProps}> {children} </Frame>;
};

Modal.Wrapper = function ModalWrapper({ children, ...restProps }) {
	return <Wrapper {...restProps}> {children} </Wrapper>;
};

Modal.Table = function ModalTable({ children, ...restProps }) {
	return <Table {...restProps}>{children}</Table>;
};

Modal.TableWrapper = function ModalTableWrapper({ children, ...restProps }) {
	return <TableWrapper {...restProps}>{children}</TableWrapper>;
};

Modal.TableHeader = function ModalTableHeader({ children, ...restProps }) {
	return <TableHeader {...restProps}>{children}</TableHeader>;
};

Modal.TableContent = function ModalTableContent({ children, ...restProps }) {
	return <TableContent {...restProps}>{children}</TableContent>;
};

Modal.Break = function ModalBreak({ children, ...restProps }) {
	return <Break {...restProps}>{children}</Break>;
};
