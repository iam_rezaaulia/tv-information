import styled from "styled-components/macro";
import Modal, { BaseModalBackground } from "styled-react-modal";

export const Background = styled(BaseModalBackground)`
	opacity: ${(props) => props.opacity};
	transition: all 0.3s ease-in-out;
`;

export const Container = Modal.styled`
  max-width: 1700px;
  height: ${(props) => props.height};
  display: flex;
  padding: 25px;
  margin: 25px;
  flex-direction: column;
  background-color: #ffffff;
  border: 1px solid #dedede;
  border-radius: 4px;
  opacity: ${(props) => props.opacity};
  overflow-y: ${(props) => props.overflow};
  transition : all 0.3s ease-in-out;
`;

export const Title = styled.p`
	font-size: 18px;
	font-weight: 600;
	margin: 0;
	color: #333333;
`;

export const Subtitle = styled.p`
	font-size: 16px;
	font-weight: 600;
	margin: 0;
	color: #333333;
`;

export const Ol = styled.ol`
	margin-top: 4px;
	padding-left: 22px;
`;

export const Li = styled.li`
	margin-top: 4px;
	margin-bottom: 6px;

	li:first-child {
		margin-top: 0;
	}
`;

export const Text = styled.p`
	font-size: 16px;
	font-weight: 500;
	color: #333333;
`;

export const Image = styled.img``;

export const Frame = styled.div`
	display: flex;
	flex-direction: row;
`;

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	margin-right: 36px;
`;

export const Table = styled.table`
	border: 1px solid #dedede;
	border-collapse: separate;
	border-left: 0;
	border-radius: 4px;
	border-spacing: 0px;
	min-width: 800px;
`;

export const TableWrapper = styled.tr`
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
`;

export const TableHeader = styled.th`
	padding: 8px;
	text-align: center;
	border-left: 1px solid #dedede;
	border-top: 1px solid #dedede;
	font-size: 16px;
	font-weight: 600;
	color: #333333;

	&:first {
		text-align: center;
	}
`;

export const TableContent = styled.td`
	padding: 5px 4px 6px 4px;
	text-align: left;
	border-left: 1px solid #dedede;
	border-top: 1px solid #dedede;

	&:first {
		text-align: center;
	}
`;

export const Break = styled.br`
	flex-basis: 100%;
	height: 0;
`;
