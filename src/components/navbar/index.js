import React from 'react';
import { Container, Link } from './navbar';

export default function Navbar({ children, ...restProps }) {
  return <Container {...restProps}> {children} </Container>;
};

Navbar.Link = function NavbarLink({ to, children, ...restProps }) {
  return <Link to={to} {...restProps}> {children} </Link>;
};