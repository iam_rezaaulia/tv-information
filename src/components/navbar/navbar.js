import styled from 'styled-components/macro';
import { NavLink } from 'react-router-dom';

const activeClassName = 'nav-item-active';

export const Link = styled(NavLink).attrs({ activeClassName })`
  &.${activeClassName} {
    border-bottom: 4px solid #34D399;
    padding-bottom: 12px;
    font-weight: 500;
    font-size: 20px;
    transition: all 150ms ease-in-out;
    color: #121212;
  }
`;

export const Container = styled.nav`
  display: flex;
  padding: 32px 0;

  a {
    color: #333333;
    text-decoration: none;
    font-weight: 500;
    font-size: 20px;
    margin-left: 3rem;
    display: block;
  }

  a:first-child {
    margin-left: 0;
  }
`;

