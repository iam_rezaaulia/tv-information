import React from "react";
import { Frame, Title, Container, Wrapper, Header, Content } from "./table";

export default function Table({ children, ...rest }) {
	return <Frame {...rest}>{children}</Frame>;
}

Table.Title = function TableTitle({ children, ...rest }) {
	return <Title {...rest}>{children}</Title>;
};

Table.Container = function TableContainer({ children, ...rest }) {
	return <Container {...rest}>{children}</Container>;
};

Table.Wrapper = function TableWrapper({ children, ...rest }) {
	return <Wrapper {...rest}>{children}</Wrapper>;
};

Table.Header = function TableHeader({ children, ...rest }) {
	return <Header {...rest}>{children}</Header>;
};

Table.Content = function TableContent({ children, ...rest }) {
	return <Content {...rest}>{children}</Content>;
};
