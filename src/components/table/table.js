import styled from "styled-components/macro";

export const Frame = styled.div``;

export const Title = styled.p``;

export const Container = styled.table``;

export const Wrapper = styled.tr``;

export const Header = styled.th``;

export const Content = styled.td``;
