export const HOME = '/';
export const LAYANAN = '/layanan';
export const PENGUMUMAN = '/pengumuman';

export const HUKUM_KEPEGAWAIAN_DAN_ORGANISASI = '/layanan/hko';
export const PERENCANAAN_EVALUASI_DAN_PELAPORAN = '/layanan/pep';
export const KEUANGAN_DAN_BARANG_MILIK_NEGARA = '/layanan/kbmn';
export const UMUM_DAN_HUMAS = '/layanan/uh';