import React from "react";
import { Button } from "../components";
import { LAYANAN } from "../constants/router";

export default function ButtonContainer() {
	return (
		<Button>
			<Button.ButtonBack to={LAYANAN}>
				<Button.IconBack /> Kembali
			</Button.ButtonBack>
		</Button>
	);
}
