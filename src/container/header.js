import React from "react";
import { Header } from "../components";

export default function HeaderContainer({ children }) {
	return (
		<Header>
			<Header.Title>
				Layanan Informasi<Header.Span>Bagian Tata Usaha</Header.Span>
			</Header.Title>
			{children}
		</Header>
	);
}
