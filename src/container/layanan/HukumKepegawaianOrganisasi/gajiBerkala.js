import React, { useState } from 'react';
import { Card, Modal } from '../../../components';
import { Gaji } from '../../../images';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Card.Container onClick={toggleModal}>
        <Card.Image src={Gaji} alt='Kenaikan Gaji Berkala' />
        <Card.Text>Kenaikan Gaji Berkala</Card.Text>
        <Card.TextSmall>Syarat Kenaikan Gaji Berkala</Card.TextSmall>
      </Card.Container>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <Modal.Title>Syarat Kenaikan Gaji Berkala</Modal.Title>
        <Modal.Ol>
          <Modal.Li>Fotokopi Gaji Berkala Terakhir 1 Lembar</Modal.Li>
          <Modal.Li>Fotokopi Pangkat Terakhir 1 Lembar</Modal.Li>
        </Modal.Ol>
      </Modal.Container>
    </>
  );
};

export default function GajiBerkala() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};
