import React, { useState } from 'react';
import { Card, Modal } from '../../../components';
import { Hukuman } from '../../../images';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Card.Container onClick={toggleModal}>
        <Card.Image src={Hukuman} alt='Hukuman Disiplin' />
        <Card.Text>Hukuman Disiplin</Card.Text>
        <Card.TextSmall>Peraturan Hukuman Disiplin</Card.TextSmall>
      </Card.Container>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <Modal.Frame>
          <Modal.Wrapper>
            <Modal.Title>Tingkat Ringan - Pasal 7 Ayat 2</Modal.Title>
            <Modal.Ol>
              <Modal.Li>Teguran Lisan</Modal.Li>
              <Modal.Li>Teguran Tertulis</Modal.Li>
              <Modal.Li>Pernyataan Tidak Puas Secara Tertulis</Modal.Li>
            </Modal.Ol>
          </Modal.Wrapper>
          <Modal.Wrapper>
            <Modal.Title>Tingkat Sedang - Pasal 7 Ayat 3</Modal.Title>
            <Modal.Ol>
              <Modal.Li>Penundaan Kenaikan Gaji Berkala Selama 1 Tahun</Modal.Li>
              <Modal.Li>Penundaan Kenaikan Pangkat Selama 1 Tahun</Modal.Li>
              <Modal.Li>Penurunan Pangkat Setingkat Lebih Rendah Selama 1 Tahun</Modal.Li>
            </Modal.Ol>
          </Modal.Wrapper>
          <Modal.Wrapper>
            <Modal.Title>Tingkat Berat - Pasal 7 Ayat 4</Modal.Title>
            <Modal.Ol>
              <Modal.Li>Penurunan Pangkat Setingkat Lebih Rendah Selama 3 Tahun</Modal.Li>
              <Modal.Li>Pemindahan Dalam Rangka Penurunan Jabatan Setingkat Lebih Rendah</Modal.Li>
              <Modal.Li>Pembebasan Dari Jabatan</Modal.Li>
              <Modal.Li>Pemberhentian Dengan Hormat Tidak Atas Permintaan Sendiri Sebagai PNS</Modal.Li>
              <Modal.Li>Pemberhentian Tidak Dengan Hormat Sebagai PNS</Modal.Li>
            </Modal.Ol>
          </Modal.Wrapper>
        </Modal.Frame>
      </Modal.Container>
    </>
  );
};

export default function HukumanDisiplin() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};
