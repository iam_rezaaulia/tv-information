import React from 'react';
import { Card } from '../../../components';
import { 
  GajiBerkala, 
  NaikPangkat, 
  KartuKepegawaian,
  KartuIstriSuami,
  Pensiun,
  HukumanDisiplin
} from './menu';

export default function MenuHukumKepegawaianOrganisasi() {
  return (
    <Card>
      <GajiBerkala />
      <NaikPangkat />
      <KartuKepegawaian />
      <KartuIstriSuami />
      <Pensiun />
      <HukumanDisiplin />
    </Card>
  );
};