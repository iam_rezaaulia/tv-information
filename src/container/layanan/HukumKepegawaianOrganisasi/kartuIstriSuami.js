import React, { useState } from 'react';
import { Card, Modal } from '../../../components';
import { KarisKarsu } from '../../../images';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Card.Container onClick={toggleModal}>
        <Card.Image src={KarisKarsu} alt='Kartu Suami Istri' />
        <Card.Text>Kartu Istri / Suami</Card.Text>
        <Card.TextSmall>Syarat Usul Kartu Istri / Suami</Card.TextSmall>
      </Card.Container>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <Modal.Title>Syarat Usul Karis / Karsu</Modal.Title>
        <Modal.Ol>
          <Modal.Li>Laporan Perkawinan Pertama</Modal.Li>
          <Modal.Li>Akta Nikah Yang Telah Dilegalisir Oleh Pihak Berwenang</Modal.Li>
          <Modal.Li>Surat Daftar Keluarga</Modal.Li>
          <Modal.Li>Pasfoto 3 Lembar Ukuran 2x3 cm</Modal.Li>
        </Modal.Ol>
      </Modal.Container>
    </>
  );
};

export default function KartuIstriSuami() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};
