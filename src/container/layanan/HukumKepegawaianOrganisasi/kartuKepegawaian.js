import React, { useState } from 'react';
import { Card, Modal } from '../../../components';
import { Kartu } from '../../../images';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Card.Container onClick={toggleModal}>
        <Card.Image src={Kartu} alt='Kartu Kepegawaian' />
        <Card.Text>Kartu Kepegawaian</Card.Text>
        <Card.TextSmall>Syarat Kartu Kepegawaian</Card.TextSmall>
      </Card.Container>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <Modal.Title>Syarat Kartu Kepegawaian</Modal.Title>
        <Modal.Ol>
          <Modal.Li>Fotokopi SK Calon Pegawai Negeri Sipil (CPNS)</Modal.Li>
          <Modal.Li>Fotokopi SK Pegawai Negeri Sipil (PNS)</Modal.Li>
          <Modal.Li>Fotokopi Surat Tanda Lulus Pendidikan dan Latihan (Prajabatan) Rangkap 3</Modal.Li>
          <Modal.Li>Pasfoto 3 Lembar Masing-Masing Ukuran 2x3 dan 3x4 cm</Modal.Li>
          <Modal.Li>Surat Pernyataan Melaksanakan Tugas (SPMT)</Modal.Li>
        </Modal.Ol>
      </Modal.Container>
    </>
  );
};

export default function KartuKepegawaian() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};
