export { default as GajiBerkala } from './gajiBerkala';
export { default as NaikPangkat } from './pangkat';
export { default as KartuKepegawaian } from './kartuKepegawaian';
export { default as KartuIstriSuami } from './kartuIstriSuami';
export { default as Pensiun } from './pensiun';
export { default as HukumanDisiplin } from './hukumanDisiplin';