import React, { useState } from 'react';
import { Card, Modal } from '../../../components';
import { Pangkat } from '../../../images';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Card.Container onClick={toggleModal}>
        <Card.Image src={Pangkat} alt='Kenaikan Gaji Berkala' />
        <Card.Text>Kenaikan Pangkat</Card.Text>
        <Card.TextSmall>Syarat Kenaikan Pangkat</Card.TextSmall>
      </Card.Container>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
        allowScroll={true}
        height='800px'
        overflow='scroll'
      >
        <Modal.Wrapper>
          <Modal.Title>Kenaikan Pangkat Reguler PNS (Jabatan Pelakasana Nonstruktural)</Modal.Title>
          <Modal.Ol>
            <Modal.Li>Fotokopi Sah Keputusan Dalam Pangkat Terakhir</Modal.Li>
            <Modal.Li>Fotokopi Sah Sasaran Kinerja, Capaian Sasaran Kinerja Dan Penilaian Prestasi Kerja Pegawai Tahun 2019 dan Tahun 2020</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Penunjukan Sebagai Pegawai Tugas Belajar Bagi PNS Yang Melaksanakan Tugas Belajar</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Penugasan Bagi Pegawai Yang Dipekerjakan Di Luar Kementerian ATR/BPN</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Tentang Pemindahan PNS Bagi Yang Pindah Wilayah Tugas</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Pengangkatan/Penetapan/Perpindahan Jabatan Pelaksana Nonstruktural</Modal.Li>
          </Modal.Ol>
        </Modal.Wrapper>
      
        <Modal.Wrapper>
          <Modal.Title>Kenaikan Pangkat Pilihan PNS (Jabatan Struktural)</Modal.Title>
          <Modal.Ol>
            <Modal.Li>Fotokopi Sah Keputusan Pengangkatan Dalam Jabatan Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Surat Pernyataan Pelantikan Dalam Jabatan Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Dalam Pangkat Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Sasaran Kinerja, Capaian Sasaran Kinerja Dan Penilaian Prestasi Kerja Pegawai Tahun 2019 dan Tahun 2020</Modal.Li>
          </Modal.Ol>
        </Modal.Wrapper>
      
        <Modal.Wrapper>
          <Modal.Title>Kenaikan Pangkat Pilihan PNS (Jabatan Fungsional)</Modal.Title>
          <Modal.Ol>
            <Modal.Li>Fotokopi Sah Keputusan Pengangkatan Dalam Jabatan Fungsional Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Pembebasan Sementara/Pemberhentian/Pengangkatan Kembali Bagi Pejabat Fungsional Yang Pernah Diberhentikan/Dibebaskan Sementara Dan Diangkat Kembali</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Dalam Pangkat Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Sasaran Kinerja, Capaian Sasaran Kinerja Dan Penilaian Prestasi Kerja Pegawai Tahun 2019 dan Tahun 2020</Modal.Li>
            <Modal.Li>Asli Penenetapan Angka Kredit (PAK) Dalam Pangkat Terakhir</Modal.Li>
          </Modal.Ol>
        </Modal.Wrapper>

        <Modal.Wrapper>
          <Modal.Title>Kenaikan Pangkat Pilihan PNS (Prestasi Kerja Luar Biasa Baiknya)</Modal.Title>
          <Modal.Ol>
            <Modal.Li>Fotokopi Sah Keputusan Pengangkatan Dalam Jabatan Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Dalam Pangkat Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Sasaran Kinerja, Capaian Sasaran Kinerja Dan Penilaian Prestasi Kerja Pegawai Tahun 2019 dan Tahun 2020 Dengan Setiap Unsur Penilaian Prestasi Kerja Bernilai Amat Baik (91) Dalam 1 Tahun Terakhir</Modal.Li>
            <Modal.Li>Fotokopi Sah Tembusan Keputusan Penetapan Prestasi Kerja Luar Biasa Baiknya Yang Ditandatangani Oleh Pejabat Pembina Kepegawaian (Menteri)</Modal.Li>
            <Modal.Li>Bukti/Evidance Prestasi Kerja Luar Biasa Baiknya (Prestasi Kerja Yang Sangat Menonjol Baiknya Yang Secara Nyata Diakui Dalam Lingkungan Kerjanya, Sehingga PNS Yang Bersangkutan Secara Nyata Menjadi Teladan Bagi Pegawai Lainnya)</Modal.Li>
          </Modal.Ol>
        </Modal.Wrapper>

        <Modal.Wrapper>
          <Modal.Title>Kenaikan Pangkat Pilihan PNS Yang Sedang Melaksanakan Tugas Belajar</Modal.Title>
          <Modal.Ol>
            <Modal.Li>Fotokopi Sah Keputusan Pengangkatan Dalam Jabatan Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Dalam Pangkat Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Penunjukan Sebagai Pegawai Tugas Belajar</Modal.Li>
            <Modal.Li>Fotokopi Sah Sasaran Kinerja, Capaian Sasaran Kinerja Dan Penilaian Prestasi Kerja Pegawai Tahun 2019 dan Tahun 2020</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Pembebasan Sementara Atau Pemberhentian Dari Jabatan Fungsional Tertentu Bagi Pegawai Tugas Belajar Yang Sebelumnya Menduduki Jabatan Fungsional Tertentu</Modal.Li>
          </Modal.Ol>
        </Modal.Wrapper>

        <Modal.Wrapper>
          <Modal.Title>Kenaikan Pangkat Pilihan PNS Yang Telah Lulus Tugas Belajar</Modal.Title>
          <Modal.Ol>
            <Modal.Li>Fotokopi Sah Keputusan Pengangkatan Dalam Jabatan Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Dalam Pangkat Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Penunjukan Sebagai Pegawai Tugas Belajar</Modal.Li>
            <Modal.Li>Fotokopi sah (Dilegalisir Oleh Perguruan Tinggi Yang Bersangkutan) Surat Tanda Tamat Belajar/Ijazah Yang Diperolehnya</Modal.Li>
            <Modal.Li>Fotokopi Sah Sasaran Kinerja, Capaian Sasaran Kinerja Dan Penilaian Prestasi Kerja Pegawai Tahun 2019 dan Tahun 2020</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Pengangkatan Kembali Dalam Jabatan Fungsional Bagi Pegawai Yang Sebelumnya Menduduki Jabatan Fungsional</Modal.Li>
            <Modal.Li>Surat Keterangan Penyetaraan Perguruan Tinggi Dari DIKTI Bagi Lulusan Perguruan Tinggi Luar Negeri</Modal.Li>
          </Modal.Ol>
        </Modal.Wrapper>

        <Modal.Wrapper>
          <Modal.Title>Kenaikan Pangkat Pilihan PNS Yang Memperoleh Surat Tanda Tamat Belajar/Ijazah Bukan Karena Tugas Belajar</Modal.Title>
          <Modal.Ol>
            <Modal.Li>Fotokopi Sah Keputusan Dalam Pangkat Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah (Dilegalisir Oleh Sekolah/Perguruan Tinggi Yang Bersangkutan) Surat Tanda Tamat Belajar/Ijazah Atau Surat Persetujuan Peningkatan Pendidikan Dari Badan Kepegawaian Negara (BKN) Bagi Yang Memperoleh Peningkatan Pendidikan</Modal.Li>
            <Modal.Li>Fotokopi Sah Sasaran Kinerja, Capaian Sasaran Kinerja Dan Penilaian Prestasi Kerja Pegawai Tahun 2019 dan Tahun 2020</Modal.Li>
            <Modal.Li>Asli Surat Keterangan Pejabat Pembina Kepegawaian Atau Pejabat Lain Serendah-Rendahnya Pejabat Pimpinan Tinggi Pratama Tentang Uraian Tugas Yang Dibebankan Kepada PNS Yang Bersangkutan</Modal.Li>
            <Modal.Li>Fotokopi Sah Surat Tanda Lulus Ujian Penyesuaian Kenaikan Pangkat (STLUPKP) atau Izin Pencantuman Gelar (IPG)</Modal.Li>
            <Modal.Li>Surat Keterangan Penyetaraan Perguruan Tinggi Dari DIKTI Bagi Lulusan Perguruan Tinggi Luar Negeri</Modal.Li>
          </Modal.Ol>
        </Modal.Wrapper>

        <Modal.Wrapper>
          <Modal.Title>Kenaikan Pangkat Pilihan PNS Yang Dipekerjakan Atau Diperbantukan Secara Penuh Di Luar Instansi Induknya Yang Diangkat Dalam Jabatan Pimpinan Yang Telah Ditetapkan Persamaan Eselonnya Atau Jabatan Fungsional</Modal.Title>
          <Modal.Ol>
            <Modal.Li>Fotokopi Sah Keputusan Pengangkatan Dalam Jabatan Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Dalam Pangkat Terkahir</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Tentang Penugasan Di Luar Instansi Induknya</Modal.Li>
            <Modal.Li>Asli Penetapan Angka Kredit (PAK) Dalam Pangkat Terakhir Bagi PNS Yang Menduduki Jabatan Fungsional</Modal.Li>
            <Modal.Li>Fotokopi Sah Sasaran Kinerja, Capaian Sasaran Kinerja Dan Penilaian Prestasi Kerja Pegawai Tahun 2019 dan Tahun 2020</Modal.Li>
          </Modal.Ol>
        </Modal.Wrapper>

        <Modal.Wrapper>
          <Modal.Title>Kenaikan Pangkat Dari :</Modal.Title>
          <Modal.Ol>
            <Modal.Li>
              Pengatur Tingkat I (II/d) Ke Penata Muda (III/a) Harus Lulus Ujian Dinas Tingkat I 
              Atau Telah Mengikuti Dan Lulus Pendidikan Dan Pelatihan Kepemimpinan Sepada/Adum/Sepala/Diklatpim Tingkat IV 
              Atau Telah Memperoleh Ijazah Sarjana (S1) Atau Diploma IV, Ijazah Dokter, Ijazah Apoteker
              Dan Ijazah Lain Yang Setara, Spesialis I, Spesialis II, Magister (S2) Atau Doktor (S3)
            </Modal.Li>
            <Modal.Li>
              Penata Tingkat I (III/d) Ke Pembina (IV/a) Harus Lulus Ujian Dinas Tingkat II
              Atau Telah Mengikuti Dan Lulus Pendidikan Dan Pelatihan Kepemimpinan Sepadya/Spama/Diklatpim Tingkat III
              Atau Telah Memperoleh Ijazah Dokter, Ijazah Apoteker 
              Dan Ijazah Lain Yang Setara, Spesialis I, Spesialis II, Magister (S2) Atau Doktor (S3)
            </Modal.Li>
          </Modal.Ol>
        </Modal.Wrapper>

        <Modal.Wrapper>
          <Modal.Title>Selain Persyaratan Yang Disebutkan Diatas, Bagi PNS Yang Diusulkan Kenaikan Pangkat Pertama Kali Sejak Diangkat Sebagai PNS Juga Melampirkan:</Modal.Title>
          <Modal.Ol>
            <Modal.Li>Fotokopi Sah Keputusan Pengangkatan Sebagai CPNS</Modal.Li>
            <Modal.Li>Fotokopi Sah Keputusan Pengangkatan Sebagai PNS</Modal.Li>
          </Modal.Ol>
        </Modal.Wrapper>

        <Modal.Wrapper>
          <Modal.Text>
            Bagi Pegawai Yang Mengalami Mutasi Jabatan Pada Tahun 2018, Tahun 2019 Dan/Atau Tahun 2020, 
            Harus Melampirkan Sasaran Kerja Dan Capaian Kerja Pada Masing-Masing Jabatan Yang Pernah Didudukinya.
          </Modal.Text>
        </Modal.Wrapper>

        <Modal.Wrapper>
          <Modal.Text>
            Untuk Setiap Capaian Kinerja Dan Penilaian Prestasi Keja Pegawai, Harus Memiliki Nilai Minimal 76,00 (Baik) Pada Setiap Unsur Penilaiannya, 
            Kecuali Untuk Kenaikan Pangkat Luar Biasa Sebagaimana Dimaksud Pada Point 4 Di Atas (Kenaikan Pangkat Pilihan PNS Yang Menunjukkan Prestasi Kerja Luar Biasa).
          </Modal.Text>
        </Modal.Wrapper>

        <Modal.Wrapper>
          <Modal.Text>
            Apabila Terdapat Usulan Kenaikan Pangkat Yang Merupakan Gabungan Dari Beberapa Kondisi Sebagaimana Tersebut Di Atas
            Maka Kelengkapan Berkas Yang Dibutuhkan Adalah Gabungan Dari Masing-Masing Kondisi.
            Misalnya Usulan Kenaikan Pangkat Terhadap Seorang PNS Yang Telah Menyelesaikan Tugas Belajar Dan Telah Diangkat Menjadin Pejabat Struktural,
            Maka Kelengkapan Berkas Yang Dibutuhkan Adalah Sebagaimana Tersebut Dalam Masing Masing Kondisi.
          </Modal.Text>
        </Modal.Wrapper>

      </Modal.Container>
    </>
  );
};

export default function KenaikanPangkat() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};
