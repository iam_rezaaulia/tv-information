import React, { useState } from 'react';
import { Card, Modal } from '../../../components';
import { Pensiun } from '../../../images';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Card.Container onClick={toggleModal}>
        <Card.Image src={Pensiun} alt='Pensiun' />
        <Card.Text>Pensiun</Card.Text>
        <Card.TextSmall>Syarat Pensiun</Card.TextSmall>
      </Card.Container>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
        allowScroll={true}
      >
        <Modal.Frame>
          <Modal.Wrapper>
            <Modal.Title>Syarat Pensiun BUP</Modal.Title>
            <Modal.Ol>
              <Modal.Li>Pasfoto 5 Lembar 3x4 cm</Modal.Li>
              <Modal.Li>Surat Keterangan SP.4 A dan SKPPS</Modal.Li>
              <Modal.Li>Daftar Perorangan Calon Penerima Pensiun (DPCP)</Modal.Li>
              <Modal.Li>Fotokopi Kartu Pegawai (Karpeg)</Modal.Li>
              <Modal.Li>Fotokopi SK Nomor Induk Pegawai (NIP) Baru</Modal.Li>
              <Modal.Li>Fotokopi SK Pengangkatan Pertama (CPNS)</Modal.Li>
              <Modal.Li>Fotokopi SK Pengangkatan Sebagai PNS</Modal.Li>
              <Modal.Li>Fotokopi SK Pangkat Terakhir</Modal.Li>
              <Modal.Li>Fotokopi Gaji Berkala Terakhir</Modal.Li>
              <Modal.Li>Fotokopi Surat Nikah Yang Telah Dilegalisir Oleh Pihak Berwenang</Modal.Li>
              <Modal.Li>Daftar Susunan Keluarga</Modal.Li>
              <Modal.Li>Fotokopi Kartu Istri (Karis)</Modal.Li>
              <Modal.Li>Fotokopi SKP Tahun 2018 dan 2019</Modal.Li>
              <Modal.Li>Surat Pernyataan Tidak Pernah Dijatuhi Hukuman Disiplin Tingkat Sedang dan Berat</Modal.Li>
              <Modal.Li>Surat Keterangan Tidak Terlibat Tindak Pidana</Modal.Li>
            </Modal.Ol>
          </Modal.Wrapper>
          <Modal.Wrapper>
            <Modal.Title>Syarat Pensiun Duda / Janda</Modal.Title>
            <Modal.Ol>
              <Modal.Li>Surat Permohonan Untuk Mendapatkan Pensiun Duda/Janda</Modal.Li>
              <Modal.Li>Daftar Susunan Keluarga</Modal.Li>
              <Modal.Li>Daftar Perorangan Calon Penerima Pensiun (DPCP)</Modal.Li>
              <Modal.Li>Surat Keterangan Meninggal Dunia</Modal.Li>
              <Modal.Li>Surat Kenaikan Gaji Berkala Terakhir</Modal.Li>
              <Modal.Li>Surat Keterangan Duda/Janda</Modal.Li>
              <Modal.Li>Fotokopi Surat Nikah Yang Telah Dilegalisir Oleh Pihak Berwenang</Modal.Li>
              <Modal.Li>Fotokopi SK Pengangkatan Pertama (CPNS)</Modal.Li>
              <Modal.Li>Fotokopi SK Pengangkatan Sebagai PNS</Modal.Li>
              <Modal.Li>Fotokopi SK Kenaikan Pangkat Terakhir Yang Telat Dilegalisir</Modal.Li>
              <Modal.Li>Fotokopi SKP Tahun Sebelumnya</Modal.Li>
              <Modal.Li>Pasfoto 5 Lembar 3x4 cm</Modal.Li>
              <Modal.Li>Pasfoto Karpeg dan Karis/Karsu</Modal.Li>
              <Modal.Li>Pasfoto KTP, KK dan Taspen Yang Telah Dilegalisir</Modal.Li>
              <Modal.Li>Surat Keterangan Tidak Terlibat Tindak Pidana</Modal.Li>
              <Modal.Li>Akta Kelahiran Anak Yang Masih Menjadi Tanggungan</Modal.Li>
            </Modal.Ol>
          </Modal.Wrapper>
        </Modal.Frame>
      </Modal.Container>
    </>
  );
};

export default function PensiunContainer() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};
