import React from 'react';
import { Card } from '../../../components';
import { 
  TunjanganIstriSuami,
  TunjanganAnak,
  KenaikanGajiBerkala,
  KenaikanPangkat,
  SKPPPensiun,
  BiayaPindahMutasi,
  PindahGajiMasuk,
  PindahGajiKeluar,
} from './menu';

export default function MenuKeuanganBadanMilikNegara() {
  return (
    <Card>
      <BiayaPindahMutasi />
      <PindahGajiMasuk />
      <PindahGajiKeluar />
      <TunjanganIstriSuami />
      <TunjanganAnak />
      <KenaikanGajiBerkala />
      <KenaikanPangkat />
      <SKPPPensiun />
    </Card>
  );
};