export { default as TunjanganIstriSuami } from './tunjanganIstriSuami';
export { default as TunjanganAnak } from './tunjanganAnak';
export { default as KenaikanGajiBerkala } from './gajiBerkala';
export { default as KenaikanPangkat } from './naikPangkat';
export { default as SKPPPensiun } from './skppPensiun';
export { default as BiayaPindahMutasi } from './pindahMutasi';
export { default as PindahGajiKeluar } from './pindahGajiKeluar';
export { default as PindahGajiMasuk } from './pindahGajiMasuk';