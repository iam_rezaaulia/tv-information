import React, { useState } from 'react';
import { Card, Modal } from '../../../components';
import { Pangkat } from '../../../images';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Card.Container onClick={toggleModal}>
        <Card.Image src={Pangkat} alt='Kenaikan Pangkat' />
        <Card.Text>Kenaikan Pangkat</Card.Text>
        <Card.TextSmall>Syarat Kenaikan Pangkat</Card.TextSmall>
      </Card.Container>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <Modal.Title>Syarat Kenaikan Pangkat</Modal.Title>
        <Modal.Ol>
          <Modal.Li>SK Kenaikan Pangkat Yang Telah Dilegalisir 2 Rangkap</Modal.Li>
        </Modal.Ol>
      </Modal.Container>
    </>
  );
};

export default function KenaikanPangkat() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};
