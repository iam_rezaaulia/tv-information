import React, { useState } from 'react';
import { Card, Modal } from '../../../components';
import { SKPP } from '../../../images';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Card.Container onClick={toggleModal}>
        <Card.Image src={SKPP} alt='SKPP Pindah Gaji (Keluar)' />
        <Card.Text>SKPP Pindah Gaji (Keluar)</Card.Text>
        <Card.TextSmall>Syarat SKPP Pindah Gaji (Keluar)</Card.TextSmall>
      </Card.Container>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <Modal.Title>Syarat SKPP Pindah Gaji (Keluar)</Modal.Title>
        <Modal.Ol>
          <Modal.Li>Fotokopi SK Mutasi Yang Telah Dilegalisir 4 Rangkap</Modal.Li>
        </Modal.Ol>
      </Modal.Container>
    </>
  );
};

export default function PindahGajiKeluar() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};
