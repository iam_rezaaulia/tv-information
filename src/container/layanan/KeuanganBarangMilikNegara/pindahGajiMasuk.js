import React, { useState } from 'react';
import { Card, Modal } from '../../../components';
import { SKPP } from '../../../images';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Card.Container onClick={toggleModal}>
        <Card.Image src={SKPP} alt='SKPP Pindah Gaji (Masuk)' />
        <Card.Text>SKPP Pindah Gaji (Masuk)</Card.Text>
        <Card.TextSmall>Syarat SKPP Pindah Gaji (Masuk)</Card.TextSmall>
      </Card.Container>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <Modal.Title>Syarat SKPP Pindah Gaji (Masuk)</Modal.Title>
        <Modal.Ol>
          <Modal.Li>Surat Keterangan Penghentian Pembayaran (SKPP) 2 Rangkap</Modal.Li>
          <Modal.Li>Surat Pernyataan Melaksanakan Tugas (SPMT) 2 Rangkap</Modal.Li>
          <Modal.Li>Surat Pernyataan Menduduki Jabatan 2 Rangkap</Modal.Li>
          <Modal.Li>Surat Pernyataan Pelantikan 2 Rangkap</Modal.Li>
          <Modal.Li>Surat Keterangan Mutasi 2 Rangkap</Modal.Li>
          <Modal.Li>Fotokopi Rekening Bank Aceh Syariah Dan BRI Syariah 1 Rangkap</Modal.Li>
          <Modal.Li>Fotokopi KTP Dan NPWP 1 Rangkap</Modal.Li>
        </Modal.Ol>
      </Modal.Container>
    </>
  );
};

export default function PindahGajiMasuk() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};
