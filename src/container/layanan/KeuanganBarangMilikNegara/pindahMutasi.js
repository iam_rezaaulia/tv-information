import React, { useState } from 'react';
import { Card, Modal } from '../../../components';
import { Mutasi } from '../../../images';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Card.Container onClick={toggleModal}>
        <Card.Image src={Mutasi} alt='Biaya Pindah Mutasi' />
        <Card.Text>Biaya Pindah Mutasi</Card.Text>
        <Card.TextSmall>Khusus Eselon II, III, IV</Card.TextSmall>
      </Card.Container>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <Modal.Title>Syarat Biaya Pindah Mutasi</Modal.Title>
        <Modal.Ol>
          <Modal.Li>Kwintasi Atau Rincian Biaya - 3 Rangkap</Modal.Li>
          <Modal.Li>Surat Perjalanan Dinas Lembar 1 - 3 Rangkap</Modal.Li>
          <Modal.Li>Surat Perjalanan Dinas Lembar 2 - 3 Rangkap</Modal.Li>
          <Modal.Li>Daftar Tunjangan Keluarga (KP4)</Modal.Li>
          <Modal.Li>Fotokopi Berita Acara Pelantikan</Modal.Li>
          <Modal.Li>Fotokopi Surat Keterangan Mutasi (Petikan)</Modal.Li>
          <Modal.Li>Fotokopi Halaman Muka Buku Rekening - 1 Rangkap</Modal.Li>
        </Modal.Ol>
      </Modal.Container>
    </>
  );
};

export default function BiayaPindahMutasi() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};
