import React, { useState } from 'react';
import { Card, Modal } from '../../../components';
import { Anak } from '../../../images';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Card.Container onClick={toggleModal}>
        <Card.Image src={Anak} alt='Tunjangan Anak' />
        <Card.Text>Tunjangan Anak</Card.Text>
        <Card.TextSmall>Syarat Tunjangan Anak</Card.TextSmall>
      </Card.Container>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <Modal.Title>Syarat Tunjangan Anak</Modal.Title>
        <Modal.Ol>
          <Modal.Li>Daftar Tunjangan Keluarga (KP4) Yang Telah Dilegalisir 1 Rangkap</Modal.Li>
          <Modal.Li>Fotokopi Akta Kelahiran Yang Telah Dilegalisir 1 Rangkap</Modal.Li>
        </Modal.Ol>
      </Modal.Container>
    </>
  );
};

export default function TunjanganAnak() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};
