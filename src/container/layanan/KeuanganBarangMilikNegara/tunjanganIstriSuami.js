import React, { useState } from 'react';
import { Card, Modal } from '../../../components';
import { IstriSuami } from '../../../images';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Card.Container onClick={toggleModal}>
        <Card.Image src={IstriSuami} alt='Tunjangan Istri Suami' />
        <Card.Text>Tunjangan Istri / Suami</Card.Text>
        <Card.TextSmall>Syarat Tunjangan Istri / Suami</Card.TextSmall>
      </Card.Container>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <Modal.Title>Syarat Tunjangan Istri / Suami</Modal.Title>
        <Modal.Ol>
          <Modal.Li>Daftar Tunjangan Keluarga (KP4) Yang Telah Dilegalisir 1 Rangkap</Modal.Li>
          <Modal.Li>Fotokopi Buku Nikah Yang Telah Dilegalisir 1 Rangkap</Modal.Li>
        </Modal.Ol>
      </Modal.Container>
    </>
  );
};

export default function TunjanganIstriSuami() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};
