import React from "react";
import styled from "styled-components/macro";

import { Card } from "../../components";
import { HKO, KBMN, PEP, UH } from "../../images";
import * as ROUTES from "../../constants/router";

const Container = styled.div`
	margin-top: 82px;
`;

export default function LayananContainer() {
	return (
		<Container>
			<Card>
				<Card.Container to={ROUTES.HUKUM_KEPEGAWAIAN_DAN_ORGANISASI}>
					<Card.Image
						src={HKO}
						alt='Hukum, Kepegawaian Dan Organisasi'
					/>
					<Card.Text>Hukum, Kepegawaian Dan Organisasi</Card.Text>
				</Card.Container>
				<Card.Container to={ROUTES.KEUANGAN_DAN_BARANG_MILIK_NEGARA}>
					<Card.Image
						src={KBMN}
						alt='Keuangan Dan Barang Milik Negara'
					/>
					<Card.Text>Keuangan Dan Barang Milik Negara</Card.Text>
				</Card.Container>
				<Card.Container to={ROUTES.PERENCANAAN_EVALUASI_DAN_PELAPORAN}>
					<Card.Image
						src={PEP}
						alt='Perencanaan, Evaluasi Dan Pelaporan'
					/>
					<Card.Text>Perencanaan, Evaluasi Dan Pelaporan</Card.Text>
				</Card.Container>
				<Card.Container to={ROUTES.UMUM_DAN_HUMAS}>
					<Card.Image src={UH} alt='Umum dan Humas' />
					<Card.Text>Umum Dan Humas</Card.Text>
				</Card.Container>
			</Card>
		</Container>
	);
}
