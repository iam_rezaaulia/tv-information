import React from "react";
import { Navbar } from "../components";
import * as ROUTES from "../constants/router";

export default function NavbarContainer() {
	return (
		<Navbar>
			<Navbar.Link to={ROUTES.LAYANAN}>Layanan</Navbar.Link>
			<Navbar.Link to={ROUTES.PENGUMUMAN}>Pengumuman</Navbar.Link>
		</Navbar>
	);
}
