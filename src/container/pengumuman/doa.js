import React, { useState } from "react";
import { Announcement, Modal } from "../../components";

function ModalCard() {
	const [isOpen, setIsOpen] = useState(false);
	const [opacity, setOpacity] = useState(0);

	function toggleModal(e) {
		setOpacity(0);
		setIsOpen(!isOpen);
	}

	function afterOpen(e) {
		setTimeout(() => {
			setOpacity(1);
		}, 100);
	}

	function beforeClose(e) {
		return new Promise((resolve) => {
			setOpacity(0);
			setTimeout(resolve, 300);
		});
	}

	return (
		<>
			<Announcement onClick={toggleModal}>
				<Announcement.Title>
					Do'a Bersama Almarhum Ayahanda Meurah Kartika Saragih, S.E
				</Announcement.Title>
				<Announcement.Wrapper>
					<Announcement.Tag>
						Keuangan Dan Barang Milik Negara
					</Announcement.Tag>
				</Announcement.Wrapper>
				<Announcement.Text>
					09.00 - Selesai | Jum'at 5 Maret 2021
				</Announcement.Text>
			</Announcement>
			<Modal.Container
				isOpen={isOpen}
				afterOpen={afterOpen}
				beforeClose={beforeClose}
				onBackgroundClick={toggleModal}
				onEscapeKeydown={toggleModal}
				opacity={opacity}
				backgroundProps={{ opacity }}
			>
				<Modal.Title>
					Do'a Bersama Almarhum Ayahanda Meurah Kartika Saragih, S.E
				</Modal.Title>
				<Modal.Text>
					Bersama ini kami umumkan bahwa akan diadakan do'a bersama
					untuk Almarhum Ayahanda Meurah Kartika Saragih, S.E pada
					hari Jum'at, 5 Maret 2021 jam 09.00 WIB bertempat di Mushola
					Kanwil BPN Aceh.
				</Modal.Text>
			</Modal.Container>
		</>
	);
}

export default function Doa() {
	return (
		<Modal>
			<ModalCard />
		</Modal>
	);
}
