import React, { useState } from "react";
import { Announcement, Modal } from "../../components";

function ModalCard() {
	const [isOpen, setIsOpen] = useState(false);
	const [opacity, setOpacity] = useState(0);

	function toggleModal(e) {
		setOpacity(0);
		setIsOpen(!isOpen);
	}

	function afterOpen(e) {
		setTimeout(() => {
			setOpacity(1);
		}, 100);
	}

	function beforeClose(e) {
		return new Promise((resolve) => {
			setOpacity(0);
			setTimeout(resolve, 300);
		});
	}

	return (
		<>
			<Announcement onClick={toggleModal}>
				<Announcement.Title>Kenaikan Berkala 2021</Announcement.Title>
				<Announcement.Wrapper>
					<Announcement.Tag>
						Hukum Kepegawaian dan Organisasi
					</Announcement.Tag>
				</Announcement.Wrapper>
				<Announcement.Text>2 Maret 2021</Announcement.Text>
			</Announcement>
			<Modal.Container
				isOpen={isOpen}
				afterOpen={afterOpen}
				beforeClose={beforeClose}
				onBackgroundClick={toggleModal}
				onEscapeKeydown={toggleModal}
				opacity={opacity}
				backgroundProps={{ opacity }}
				allowScroll={true}
			>
				<Modal.Frame>
					<Modal.Wrapper>
						<Modal.Subtitle>TMT Februari 2021</Modal.Subtitle>
						<Modal.Ol>
							<Modal.Li>Muhammad Reza, S.T., M.Si</Modal.Li>
						</Modal.Ol>
					</Modal.Wrapper>
					<Modal.Wrapper>
						<Modal.Subtitle>TMT Maret 2021</Modal.Subtitle>
						<Modal.Ol>
							<Modal.Li>Mohamad Arfath Satya, A.Ptnh</Modal.Li>
							<Modal.Li>Devi Silfiana, S.T</Modal.Li>
							<Modal.Li>Bambang Heru Purnomo, A.Md</Modal.Li>
							<Modal.Li>Azwaruddin, A.Md</Modal.Li>
							<Modal.Li>Riska Nuriza, S.H</Modal.Li>
							<Modal.Li>Wardiah</Modal.Li>
							<Modal.Li>Prihatini Pribadi, A.Ptnh, M.Si</Modal.Li>
							<Modal.Li>Patimah Zahara</Modal.Li>
							<Modal.Li>
								Arinaldi, S.SiT, S.H., M.M.S.SiT
							</Modal.Li>
							<Modal.Li>Meriherawari</Modal.Li>
							<Modal.Li>Fahmi Riza, S.H</Modal.Li>
							<Modal.Li>Bakri</Modal.Li>
							<Modal.Li>Basyarul Azis</Modal.Li>
							<Modal.Li>Frederik Edison, S.H</Modal.Li>
							<Modal.Li>Sri Indrawati, S.E</Modal.Li>
							<Modal.Li>Ir. Joko Suprapto, M.P</Modal.Li>
						</Modal.Ol>
					</Modal.Wrapper>
					<Modal.Wrapper>
						<Modal.Subtitle>TMT April 2021</Modal.Subtitle>
						<Modal.Ol>
							<Modal.Li>Husni, S.ST</Modal.Li>
							<Modal.Li>Edo Prayuda</Modal.Li>
							<Modal.Li>Vandiyana Perwita, K, S.H</Modal.Li>
							<Modal.Li>Bambang Kurniawan</Modal.Li>
							<Modal.Li>Marlinda, A.Md</Modal.Li>
						</Modal.Ol>
					</Modal.Wrapper>
					<Modal.Wrapper>
						<Modal.Subtitle>TMT Desember 2021</Modal.Subtitle>
						<Modal.Ol>
							<Modal.Li>Farah Diba, S.P</Modal.Li>
							<Modal.Li>Febrianto</Modal.Li>
							<Modal.Li>Sri Indri Yanti, S.E</Modal.Li>
							<Modal.Li>Mailza, SE</Modal.Li>
							<Modal.Li>Lukman</Modal.Li>
							<Modal.Li>Armia</Modal.Li>
							<Modal.Li>Wahyuni, S.E</Modal.Li>
							<Modal.Li>Samsul Bahri</Modal.Li>
							<Modal.Li>Saiful</Modal.Li>
							<Modal.Li>T. Pitra Mulia, S.H</Modal.Li>
							<Modal.Li>Ideram</Modal.Li>
							<Modal.Li>Yusri</Modal.Li>
						</Modal.Ol>
					</Modal.Wrapper>
				</Modal.Frame>
			</Modal.Container>
		</>
	);
}

export default function KenaikanGajiBerkala() {
	return (
		<Modal>
			<ModalCard />
		</Modal>
	);
}
