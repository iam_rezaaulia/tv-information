import React, { useState } from "react";
import { Announcement, Modal } from "../../components";

function ModalCard() {
	const [isOpen, setIsOpen] = useState(false);
	const [opacity, setOpacity] = useState(0);

	function toggleModal(e) {
		setOpacity(0);
		setIsOpen(!isOpen);
	}

	function afterOpen(e) {
		setTimeout(() => {
			setOpacity(1);
		}, 100);
	}

	function beforeClose(e) {
		return new Promise((resolve) => {
			setOpacity(0);
			setTimeout(resolve, 300);
		});
	}

	return (
		<>
			<Announcement onClick={toggleModal}>
				<Announcement.Title>
					Naik Pangkat Per April 2021
				</Announcement.Title>
				<Announcement.Wrapper>
					<Announcement.Tag>
						Hukum Kepegawaian dan Organisasi
					</Announcement.Tag>
				</Announcement.Wrapper>
				<Announcement.Text>1 Maret 2021</Announcement.Text>
			</Announcement>
			<Modal.Container
				isOpen={isOpen}
				afterOpen={afterOpen}
				beforeClose={beforeClose}
				onBackgroundClick={toggleModal}
				onEscapeKeydown={toggleModal}
				opacity={opacity}
				backgroundProps={{ opacity }}
				allowScroll={true}
			>
				<Modal.Subtitle>
					Berikut nama-nama pegawai yang naik pangkat per april 2021
				</Modal.Subtitle>
				<Modal.Wrapper>
					<Modal.Ol>
						<Modal.Li>Didin Sihabudin, S.H</Modal.Li>
						<Modal.Li>T. Alamsah, S.H</Modal.Li>
						<Modal.Li>Vandiyana Perwita Kusuma, S.H</Modal.Li>
						<Modal.Li>Hanni Harbimaharani, S.T</Modal.Li>
						<Modal.Li>Mailiza, S.E</Modal.Li>
						<Modal.Li>Marlinda, A.Md</Modal.Li>
						<Modal.Li>Sri Indri Yanti, S.E</Modal.Li>
						<Modal.Li>Azwaruddin, A.Md</Modal.Li>
						<Modal.Li>Rivelga Deandimi, A.Md</Modal.Li>
						<Modal.Li>Mila Hayati, S.H</Modal.Li>
						<Modal.Li>Meurah Kartika Saragih, S.E</Modal.Li>
						<Modal.Li>Bambang Heru Purnomo, A.Md</Modal.Li>
						<Modal.Li>Mulyadi, S.H</Modal.Li>
						<Modal.Li>Budhi Setiawan</Modal.Li>
						<Modal.Li>Sri Wahyuna</Modal.Li>
					</Modal.Ol>
				</Modal.Wrapper>
				<Modal.Subtitle>
					Untuk persyaratannya dapat dilihat pada menu Layanan.
				</Modal.Subtitle>
			</Modal.Container>
		</>
	);
}

export default function KenaikanGajiBerkala() {
	return (
		<Modal>
			<ModalCard />
		</Modal>
	);
}
