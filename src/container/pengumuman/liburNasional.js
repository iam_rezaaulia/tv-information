import React, { useState } from "react";
import { Announcement, Modal } from "../../components";

function ModalCard() {
	const [isOpen, setIsOpen] = useState(false);
	const [opacity, setOpacity] = useState(0);

	function toggleModal(e) {
		setOpacity(0);
		setIsOpen(!isOpen);
	}

	function afterOpen(e) {
		setTimeout(() => {
			setOpacity(1);
		}, 100);
	}

	function beforeClose(e) {
		return new Promise((resolve) => {
			setOpacity(0);
			setTimeout(resolve, 300);
		});
	}

	return (
		<React.Fragment>
			<Announcement onClick={toggleModal}>
				<Announcement.Title>
					Hari Libur Nasional & Cuti Bersama Tahun 2021
				</Announcement.Title>
				<Announcement.Wrapper>
					<Announcement.Tag>
						Hukum Kepegawaian dan Organisasi
					</Announcement.Tag>
				</Announcement.Wrapper>
				<Announcement.Text>1 Maret 2021</Announcement.Text>
			</Announcement>
			<Modal.Container
				isOpen={isOpen}
				afterOpen={afterOpen}
				beforeClose={beforeClose}
				onBackgroundClick={toggleModal}
				onEscapeKeydown={toggleModal}
				opacity={opacity}
				backgroundProps={{ opacity }}
				// height='800px'
				// overflow='scroll'
			>
				<Modal.Wrapper>
					<Modal.Title>Hari Libur Nasional Tahun 2021</Modal.Title>
					<Modal.Break />
					<Modal.Table>
						<Modal.TableWrapper>
							<Modal.TableHeader>No.</Modal.TableHeader>
							<Modal.TableHeader>Tanggal</Modal.TableHeader>
							<Modal.TableHeader>Hari</Modal.TableHeader>
							<Modal.TableHeader>Keterangan</Modal.TableHeader>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>1.</Modal.TableContent>
							<Modal.TableContent>1 Januari</Modal.TableContent>
							<Modal.TableContent>Jum'at</Modal.TableContent>
							<Modal.TableContent>
								Tahun Baru 2021 Masehi
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>2.</Modal.TableContent>
							<Modal.TableContent>12 Februari</Modal.TableContent>
							<Modal.TableContent>Jum'at</Modal.TableContent>
							<Modal.TableContent>
								Tahun Baru Imlek 2572 Kongzili
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>3.</Modal.TableContent>
							<Modal.TableContent>11 Maret</Modal.TableContent>
							<Modal.TableContent>Kamis</Modal.TableContent>
							<Modal.TableContent>
								Isra Mi'raj Nabi Muhammad SAW
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>4.</Modal.TableContent>
							<Modal.TableContent>14 Maret</Modal.TableContent>
							<Modal.TableContent>Minggu</Modal.TableContent>
							<Modal.TableContent>
								Hari Suci Nyepi Tahun Baru Saka 1943
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>5.</Modal.TableContent>
							<Modal.TableContent>2 Arpil</Modal.TableContent>
							<Modal.TableContent>Jum'at</Modal.TableContent>
							<Modal.TableContent>
								Wafat Isa Al Masih
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>6.</Modal.TableContent>
							<Modal.TableContent>1 Mei</Modal.TableContent>
							<Modal.TableContent>Sabtu</Modal.TableContent>
							<Modal.TableContent>
								Hari Buruh Nasional
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>7.</Modal.TableContent>
							<Modal.TableContent>13 Mei</Modal.TableContent>
							<Modal.TableContent>Kamis</Modal.TableContent>
							<Modal.TableContent>
								Kenaikan Isa Al Masih
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>8.</Modal.TableContent>
							<Modal.TableContent>13 - 14 Mei</Modal.TableContent>
							<Modal.TableContent>
								Kamis - Jum'at
							</Modal.TableContent>
							<Modal.TableContent>
								Hari Raya Idul Fitri 1442 Hijriyah
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>9.</Modal.TableContent>
							<Modal.TableContent>26 Mei</Modal.TableContent>
							<Modal.TableContent>Rabu</Modal.TableContent>
							<Modal.TableContent>
								Hari Raya Waisak 2565
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>10.</Modal.TableContent>
							<Modal.TableContent>1 Juni</Modal.TableContent>
							<Modal.TableContent>Selasa</Modal.TableContent>
							<Modal.TableContent>
								Hari Lahir Pancasila
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>11.</Modal.TableContent>
							<Modal.TableContent>20 Juli</Modal.TableContent>
							<Modal.TableContent>Selasa</Modal.TableContent>
							<Modal.TableContent>
								Hari Raya Idul Adha 1442 Hijriyah
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>12.</Modal.TableContent>
							<Modal.TableContent>10 Agustus</Modal.TableContent>
							<Modal.TableContent>Selasa</Modal.TableContent>
							<Modal.TableContent>
								Tahun Baru Islam 1443 Hijriyah
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>13.</Modal.TableContent>
							<Modal.TableContent>17 Agustus</Modal.TableContent>
							<Modal.TableContent>Selasa</Modal.TableContent>
							<Modal.TableContent>
								Hari Kemerdekaan Republik Indonesia
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>14.</Modal.TableContent>
							<Modal.TableContent>19 Oktober</Modal.TableContent>
							<Modal.TableContent>Selasa</Modal.TableContent>
							<Modal.TableContent>
								Maulid Nabi Muhammad SAW
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>15.</Modal.TableContent>
							<Modal.TableContent>25 Desember</Modal.TableContent>
							<Modal.TableContent>Sabtu</Modal.TableContent>
							<Modal.TableContent>
								Hari Raya Natal
							</Modal.TableContent>
						</Modal.TableWrapper>
					</Modal.Table>
				</Modal.Wrapper>
				<Modal.Break />
				<Modal.Wrapper>
					<Modal.Title>Cuti Bersama Tahun 2021</Modal.Title>
					<Modal.Break />
					<Modal.Table>
						<Modal.TableWrapper>
							<Modal.TableHeader>No.</Modal.TableHeader>
							<Modal.TableHeader>Tanggal</Modal.TableHeader>
							<Modal.TableHeader>Hari</Modal.TableHeader>
							<Modal.TableHeader>Keterangan</Modal.TableHeader>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>1.</Modal.TableContent>
							<Modal.TableContent>12 Mei</Modal.TableContent>
							<Modal.TableContent>Rabu</Modal.TableContent>
							<Modal.TableContent>
								Hari Raya Idul Fitri 1442 Hijriyah
							</Modal.TableContent>
						</Modal.TableWrapper>
						<Modal.TableWrapper>
							<Modal.TableContent>2.</Modal.TableContent>
							<Modal.TableContent>24 Desember</Modal.TableContent>
							<Modal.TableContent>Jum'at</Modal.TableContent>
							<Modal.TableContent>
								Hari Raya Natal
							</Modal.TableContent>
						</Modal.TableWrapper>
					</Modal.Table>
				</Modal.Wrapper>
			</Modal.Container>
		</React.Fragment>
	);
}

const MemoizedModalCard = React.memo(ModalCard);

export default function LiburNasional() {
	return (
		<Modal>
			<MemoizedModalCard />
		</Modal>
	);
}
