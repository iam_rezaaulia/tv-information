import React, { useState } from 'react';
import { Announcement, Modal } from '../../components';

function ModalCard() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setOpacity(0);
    setIsOpen(!isOpen);
  }

  function afterOpen(e) {
    setTimeout(() => {
      setOpacity(1);
    }, 100);
  }

  function beforeClose(e) {
    return new Promise((resolve) => {
      setOpacity(0);
      setTimeout(resolve, 300);
    });
  }

  return (
    <>
      <Announcement onClick={toggleModal}>
        <Announcement.Title>Tunjangan Kinerja Januari 2021</Announcement.Title>
        <Announcement.Wrapper>
          <Announcement.Tag>Keuangan Dan Barang Milik Negara</Announcement.Tag>
        </Announcement.Wrapper>
        <Announcement.Text>15 Februari 2021</Announcement.Text>
      </Announcement>
      <Modal.Container
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <Modal.Title>Tunjangan Kinerja Januari 2021</Modal.Title>
        <Modal.Text>Bersama ini kami umumkan bahwa Tunjangan Kinerja Bulan Januari 2021 sudah dikirim ke rekening masing-masing pegawai (BRI Syariah)</Modal.Text>
      </Modal.Container>
    </>
  );
};

export default function TunjanganKinerja() {
  return (
    <Modal>
      <ModalCard />
    </Modal>
  );
};

