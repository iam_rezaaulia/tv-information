import React, { useState } from "react";
import { Announcement, Modal } from "../../components";
import { Walimah1 } from "../../images/pengumuman";

function ModalCard() {
	const [isOpen, setIsOpen] = useState(false);
	const [opacity, setOpacity] = useState(0);

	function toggleModal(e) {
		setOpacity(0);
		setIsOpen(!isOpen);
	}

	function afterOpen(e) {
		setTimeout(() => {
			setOpacity(1);
		}, 100);
	}

	function beforeClose(e) {
		return new Promise((resolve) => {
			setOpacity(0);
			setTimeout(resolve, 300);
		});
	}

	return (
		<>
			<Announcement onClick={toggleModal}>
				<Announcement.Title>
					Nikah dan Resepsi Sabila Desvi, S.T
				</Announcement.Title>
				<Announcement.Wrapper>
					<Announcement.Tag>Undangan Nikah</Announcement.Tag>
				</Announcement.Wrapper>
				<Announcement.Text>12 & 13 Maret 2021</Announcement.Text>
			</Announcement>
			<Modal.Container
				isOpen={isOpen}
				afterOpen={afterOpen}
				beforeClose={beforeClose}
				onBackgroundClick={toggleModal}
				onEscapeKeydown={toggleModal}
				opacity={opacity}
				backgroundProps={{ opacity }}
			>
				<Modal.Image src={Walimah1} alt='Walimah Sabila Desvi' />
			</Modal.Container>
		</>
	);
}

const MemoizedModalCard = React.memo(ModalCard);

export default function UndanganNikah() {
	return (
		<Modal>
			<MemoizedModalCard />
		</Modal>
	);
}
