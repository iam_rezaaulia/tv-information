import React, { useState } from "react";
import { Announcement, Modal } from "../../components";
import { Webinar } from "../../images/pengumuman";

function ModalCard() {
	const [isOpen, setIsOpen] = useState(false);
	const [opacity, setOpacity] = useState(0);

	function toggleModal(e) {
		setOpacity(0);
		setIsOpen(!isOpen);
	}

	function afterOpen(e) {
		setTimeout(() => {
			setOpacity(1);
		}, 100);
	}

	function beforeClose(e) {
		return new Promise((resolve) => {
			setOpacity(0);
			setTimeout(resolve, 300);
		});
	}

	return (
		<>
			<Announcement onClick={toggleModal}>
				<Announcement.Title>
					How To Successfully Passing English Tests And Academic Tests
					To Open Up Global Opportunities
				</Announcement.Title>
				<Announcement.Wrapper>
					<Announcement.Tag>
						Webinar Aparatur Muda Unggul ATR/BPN
					</Announcement.Tag>
				</Announcement.Wrapper>
				<Announcement.Text>
					08.00 - 11.45 | 9 Maret 2021
				</Announcement.Text>
			</Announcement>
			<Modal.Container
				isOpen={isOpen}
				afterOpen={afterOpen}
				beforeClose={beforeClose}
				onBackgroundClick={toggleModal}
				onEscapeKeydown={toggleModal}
				opacity={opacity}
				backgroundProps={{ opacity }}
				height='800px'
				overflow='scroll'
			>
				<Modal.Image src={Webinar} alt='Webinar Aparatur Muda Unggul' />
			</Modal.Container>
		</>
	);
}

const MemoizedModalCard = React.memo(ModalCard);

export default function WebinarATRBPN() {
	return (
		<Modal>
			<MemoizedModalCard />
		</Modal>
	);
}
