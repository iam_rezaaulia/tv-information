import { createGlobalStyle } from 'styled-components/macro';

export const GlobalStyle = createGlobalStyle`
  html, body {
    max-width: 1600px;
    margin: 0 auto;
    padding: 0;
    font-family: 'Poppins', sans-serif;
    background: #F5F6F8;
    box-sizing: border-box;
    scroll-behavior: smooth;
  }
`;