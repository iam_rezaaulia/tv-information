// Layanan Bidang
export { default as HKO } from './hko.png';
export { default as KBMN } from './kbmn.png';
export { default as PEP } from './pep.png';
export { default as UH } from './uh.png';

// Layanan HKO
export { default as Gaji } from './gaji.png';
export { default as Pangkat } from './pangkat.png';
export { default as Kartu } from './kartu.png';
export { default as KarisKarsu } from './karis-karsu.png';
export { default as Pensiun } from './pensiun.png';
export { default as Hukuman } from './hukuman.png';

// Layanan KBMN
export { default as SKPP } from './skpp.png';
export { default as Mutasi } from './mutasi.png';
export { default as Anak } from './tunjangan-anak.png';
export { default as IstriSuami } from './istri-suami.png';
export { default as PensiunSKPP } from './skpp-pensiun.png';