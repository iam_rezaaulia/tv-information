import React from "react";
import { HeaderContainer, HKOContainer, ButtonContainer } from "../container";

export default function HKO() {
	return (
		<>
			<HeaderContainer />
			<ButtonContainer />
			<HKOContainer />
		</>
	);
}
