export { default as Pengumuman } from './pengumuman';
export { default as Layanan } from './layanan';
export { default as HKO } from './hko';
export { default as KBMN } from './kbmn';
export { default as PEP } from './pep';
export { default as UH } from './uh';