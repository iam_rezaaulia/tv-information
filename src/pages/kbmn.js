import React from "react";
import { HeaderContainer, KBMNContainer, ButtonContainer } from "../container";

export default function KBMN() {
	return (
		<>
			<HeaderContainer />
			<ButtonContainer />
			<KBMNContainer />
		</>
	);
}
