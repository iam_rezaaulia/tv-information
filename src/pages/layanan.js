import React from 'react';
import { HeaderContainer, LayananContainer } from '../container';

export default function Layanan() {
  return (
    <>
      <HeaderContainer />
      <LayananContainer />
    </>
  );
};
