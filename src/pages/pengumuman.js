import React from "react";
import styled from "styled-components/macro";
import {
	KenaikanGajiBerkala,
	KenaikanPangkat,
	// TunjanganKinerja,
	// UangMakan,
	Webinar,
	Walimah,
	Walimah2,
	Doa,
	LiburNasional,
} from "../container/pengumuman";

const Frame = styled.div`
	display: flex;
	flex-wrap: wrap;
`;

export default function Pengumuman() {
	return (
		<Frame>
			<Walimah />
			<Walimah2 />
			<Webinar />
			<Doa />
			<KenaikanGajiBerkala />
			<KenaikanPangkat />
			<LiburNasional />
			{/* <UangMakan /> */}
			{/* <TunjanganKinerja /> */}
		</Frame>
	);
}
