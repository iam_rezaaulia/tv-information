import React from "react";
import { HeaderContainer, PEPContainer, ButtonContainer } from "../container";

export default function PEP() {
	return (
		<>
			<HeaderContainer />
			<ButtonContainer />
			<PEPContainer />
		</>
	);
}
