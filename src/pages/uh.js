import React from "react";
import { HeaderContainer, UHContainer, ButtonContainer } from "../container";

export default function UH() {
	return (
		<>
			<HeaderContainer />
			<ButtonContainer />
			<UHContainer />
		</>
	);
}
